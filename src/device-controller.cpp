#include "device-controller.hpp"
#include "logger.hpp"

#include <ndn-cxx/encoding/block-helpers.hpp>
#include <ndn-cxx/encoding/tlv.hpp>
#include <ndn-cxx/security/signing-helpers.hpp>
#include <ndn-cxx/lp/tags.hpp>

namespace ndn {
namespace iot {

static const Name DISCOVERY_PREFIX("/localhop/probe-device");
  
DeviceController::DeviceController(const std::string& pin, const Name& name)
  : Entity(name, true)
  , m_pin(pin)
  , m_faceMonitor(m_face)
  , m_asFaceId(0)
  , m_isConsumer(false)
{
  LOG_WELCOME("IoT Device Controller", m_name);
  
  registerCommandHandler("localhop", "probe-device",
  			 bind(&DeviceController::handleProbe, this, _1, _2, _3),
  			 SecurityOptions().addOption(m_pin));

  m_agent.registerTopPrefix(DISCOVERY_PREFIX,
			    bind(&DeviceController::discovery, this, bind([] {})));
}

DeviceController&
DeviceController::registerService(const Name& prefix,
				  const CommandHandler& handler)
{
  Name fullPrefix = m_name.append(prefix);
  m_ownServices.push_back(fullPrefix);

  registerCommandHandler(m_name, prefix, handler,
			 SecurityOptions().setOption(m_name));

  return *this;
}

void
DeviceController::discovery(const serviceDiscoveryCallback& afterDiscovery)
{
  LOG_STEP("", "discovery other devices");
  
  auto command = makeCommand(DISCOVERY_PREFIX,
			     ControlParameters().setName(m_name).setVersion(m_version),
			     [this] (Interest& interest, KeyChain& keyChain) {
			       m_keyChain.sign(interest,
					       signingByIdentity(m_identity));
			     });

  command.setInterestLifetime(time::seconds(2));
  
  m_agent.broadcast(command,
		    bind(&DeviceController::onDiscoveredDevice, this, _2, afterDiscovery),
		    [] (const Interest&, const lp::Nack& nack) {
		      LOG_FAILURE("discovery", "NACK: " << nack.getReason());
		    },
		    [] (const Interest&) {
		      LOG_FAILURE("discovery", "Request TIMEOUT");
		    });
}

void
DeviceController::onDiscoveredDevice(const Data& data,
				     const serviceDiscoveryCallback& afterDiscovery)
{
  LOG_DATA_IN(data);
  
  auto content = data.getContent();
  try {
    content.parse();
  }
  catch (const tlv::Error& e) {
    LOG_FAILURE("discovery", "Can not parse the response");
    return;
  }
  
  Name devName;
  try {
    devName.wireDecode(content.get(tlv::Name));
  }
  catch (const tlv::Error& e) {
    LOG_FAILURE("discovery", "can not parse the name of device");
    return;
  }

  std::vector<Name> services;
  try {
    auto serviceBlock = content.get(tlv::iot::Services);

    serviceBlock.parse();
    for (auto service : serviceBlock.elements()) {
      services.push_back(Name(service));
    }
  }
  catch (const tlv::Error& e) {
    LOG_FAILURE("discovery", "Can not parse the service block");
  }

  if (m_services.find(devName) != m_services.end()) {
    LOG_FAILURE("discovery", "NO NEW DEVICE DETECTED, STOP DISCOVERING");
    return;
  }

  LOG_DISCOVERY(devName, services);
  m_services[devName] = services;
  m_version += ":";
  m_version += devName.toUri();
  
  afterDiscovery(services);
  
  discovery(afterDiscovery);
}

void
DeviceController::handleProbe(const ControlParameters& parameters,
			      const ReplyWithContent& done,
			      SecurityOptions options)
{ 
  if (!parameters.hasName()) {
    LOG_FAILURE("probe", "do not have name");
    return;
  }

  if (!parameters.hasVersion()) {
    LOG_FAILURE("probe", "do not have version");
    return;
  }
  
  Name requester = parameters.getName();
  auto version = parameters.getVersion();

  LOG_INFO("A trusted requester [" << requester << "] is probing me!");

  if (version.find(m_name.toUri()) != std::string::npos) {
    LOG_FAILURE("probe", "Ignore duplicate requests");
    return;
  }
    
  m_requesters[requester] = true;
    
  if (options.getVerificationType() == SecurityOptions::HMAC) {

    LOG_INFO("A trusted requester [" << parameters.getName() << "] is probing me!");
  
    auto filter = nfd::FaceQueryFilter()
      .setLinkType(nfd::LINK_TYPE_MULTI_ACCESS)
      .setFaceScope(nfd::FACE_SCOPE_NON_LOCAL);

    m_controller.fetch<nfd::FaceQueryDataset>(
      filter,
      bind(&DeviceController::makeProbeResponse, this, _1, done),
      [this, done] (uint32_t code, const std::string& reason) {
        done(ControlResponse(code, reason).wireEncode());
      });
  
    LOG_STEP(1.2, "Handle probing Interest");
    LOG_DBG("start monitor the face changes");
    m_faceMonitor.onNotification.connect(bind(&DeviceController::handleFaceCreation, this,
					      _1, parameters.getName()));

    m_faceMonitor.start();
  }
  else {
    auto content = makeEmptyBlock(tlv::Content);
    content.push_back(m_name.wireEncode());

    auto services = makeEmptyBlock(tlv::iot::Services);
    for (const auto& entry : m_ownServices) {
      Name name(entry);
      services.push_back(name.wireEncode());
    }
    content.push_back(services);

    done(content);
  }
}

void
DeviceController::handleFaceCreation(const nfd::FaceEventNotification& notification,
				     const Name& name)
{
  if (notification.getKind() == nfd::FACE_EVENT_CREATED &&
      notification.getFaceScope() != nfd::FACE_SCOPE_LOCAL &&
      notification.getFacePersistency() == nfd::FACE_PERSISTENCY_ON_DEMAND) {

      LOG_DBG("new notification of face creation: " << notification.getFaceId());
      m_createdFaces.push_back(notification.getFaceId());

      auto onFailure = [] (const nfd::ControlResponse& resp) {
	LOG_FAILURE("register route", "Error " << resp.getCode()
		    << " when registering rout to the created face: "
		    << resp.getText());
      };

      LOG_DBG("register " << name << " to face: " << notification.getFaceId());
      m_asFaceId = notification.getFaceId();
      registerPrefixOnFace("/iot", notification.getFaceId(),
			   bind(&DeviceController::applyForCertificate, this,
				name, notification.getFaceId()),
			   onFailure);

      m_faceMonitor.stop();
    }  
}

void
DeviceController::applyForCertificate(const Name& name, uint64_t faceId)
{
  LOG_STEP(2.1, "Apply AS-signed certificate from " << name);

  security::Key key;
  try {
    key = m_identity.getDefaultKey();
  }
  catch (const security::Pib::Error&) {
    key = m_keyChain.createKey(m_identity);
  }

  auto prefix = Name(name).append("apply-cert").append(m_name);
  auto params = ControlParameters().setName(key.getName()).setKey(key.getPublicKey());

  issueCommand(makeCommand(prefix, params, bind(&hmac::signInterest, _1, m_pin)),
	       bind(&DeviceController::handleApplyResponse, this,
		    key.getName(), faceId, _1),
	       bind(&hmac::verifyData, _1, m_pin));
}

void
DeviceController::handleApplyResponse(const Name& keyName, uint64_t faceId,
				      const Block& content)
{
  try {
    security::v2::Certificate anchorCert(content.blockFromValue());
    LOG_STEP(2.2, "Receive and Set trust anchor: " << anchorCert.getKeyName());
    
    m_certificates[anchorCert.getKeyName()] = anchorCert;

    registerPrefixOnFace(keyName, faceId,
			 bind(&DeviceController::requestCertificate, this,
			      keyName),
			 bind([] {
			     LOG_FAILURE("register route", "fail");
			   }));
 
  }
  catch (const tlv::Error& e) {
    LOG_FAILURE("cert", " fail to parse anchor " << e.what());
  }
}

void
DeviceController::requestCertificate(const Name& keyName)
{
  LOG_STEP(3, "Request for AS signed certificate: " << keyName);
  
  Interest interest(keyName);
  LOG_INTEREST_OUT(interest);

  interest.setInterestLifetime(time::seconds(3600));
  m_face.expressInterest(interest,
			 [this, keyName] (const Interest&, const Data& data) {
			   LOG_DATA_IN(data);
			   auto key = m_identity.getKey(keyName);
			   security::v2::Certificate cert(data);

			   m_keyChain.setDefaultCertificate(key, cert);
			   LOG_DBG("new cert installed " << cert.getName());

			   discovery(bind(&DeviceController::useService, this, _1));
			 },
			 [] (const Interest&, const lp::Nack& nack) {
			   LOG_FAILURE("request for cert", "Nack " << nack.getReason());
			 },
			 [] (const Interest&) {
			   LOG_FAILURE("request for cert", "Timeout");
			 });  
}

void
DeviceController::useService(const std::vector<Name>& services)
{
  if (!m_isConsumer) {
    return;
  }
  
  DataCallback onData = [] (const Interest&, const Data& data) {
    std::cout << data << std::endl;
  };
  NackCallback onNack = [] (const Interest&, const lp::Nack&) {
    std::cout << "service nack" << std::endl;
  };
  TimeoutCallback onTimeout = [] (const Interest&) {
    std::cout << "service timeout" << std::endl;
  };
  
  std::string name = "";
  for (const auto& e : services) {
    name = e.toUri();
    if (name.find("now") != std::string::npos) {
      m_face.expressInterest(Interest(e), onData, onNack, onTimeout);
    }
  }
}
  
void
DeviceController::makeProbeResponse(const std::vector<nfd::FaceStatus>& dataset,
				    const ReplyWithContent& done)
{
  auto content = makeEmptyBlock(tlv::Content);
  content.push_back(m_name.wireEncode());
  content.push_back(packageAccessibleUris(dataset));

  content.encode();
  done(content);
}

Block
DeviceController::packageAccessibleUris(const std::vector<nfd::FaceStatus>& dataset)
{
  auto block = makeEmptyBlock(tlv::iot::DeviceUris);
  
  if (dataset.empty()) {
    LOG_FAILURE("fetch faces", "No faces available");
    return block;
  }

  for (const auto& faceStatus : dataset) {
    auto uri = faceStatus.getLocalUri();
    auto sub = uri.substr(0, uri.find_last_of(":"));
    if (sub != uri && sub.find(":") != std::string::npos) {
      uri = sub;
    }
    if (uri.find("udp") == 0) {
      uri.replace(0, 3, "tcp");
    }
    
    if (!sub.empty()) {
      block.push_back(makeStringBlock(tlv::iot::DeviceUri, uri));
    }
  }

  block.encode();
  return block;
}

} // namespace iot
} // namespace ndn
